import {
  WiCloudy,
  WiDaySunny,
  WiRain,
  WiSnow,
  WiDayHaze,
  WiDayThunderstorm,
  WiFog,
} from "react-icons/wi";

export const ConvertToIcon = (data) => {
  let icon;
  switch (data) {
    case "Clouds":
      icon = <WiCloudy />;
      break;
    case "Haze":
      icon = <WiDayHaze />;
      break;
    case "Rain":
      icon = <WiRain className="text-[#31cafb]" />;
      break;
    case "Clear":
      icon = <WiDaySunny className="text-[#ffde33]" />;
      break;
    case "Snow":
      icon = <WiSnow className="text-[#31cafb]" />;
      break;
    case "Thunderstorm":
      icon = <WiDayThunderstorm />;
      break;
    case "Fog":
      icon = <WiFog />;
      break;
  }
  return icon;
};
