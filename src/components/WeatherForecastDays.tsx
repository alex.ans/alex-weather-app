import { ConvertToIcon } from "../utils/utils";
import { RiCelsiusLine } from "react-icons/ri";
const WeatherForecastDay = ({ props }) => {
  return (
    <div className="flex items-center  m-5 justify-center lg:flex-col lg:flex-wrap">
      {props.map((d, i) => {
        return (
          <div
            key={i}
            className="flex border border-gray-300 rounded-lg p-3 m-2"
          >
            <span className="text-2xl"> {d.label}</span>
            <span className="text-2xl">{d.data}</span>
            <span className="text-xl">
              <RiCelsiusLine />
            </span>
            <span className="text-6xl">{ConvertToIcon(d.icon)}</span>
          </div>
        );
      })}
    </div>
  );
};
export default WeatherForecastDay;
