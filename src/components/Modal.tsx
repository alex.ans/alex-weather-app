import { useContext } from "react";

import WeatherWidget from "./WeatherWidget";
import { RiCloseCircleLine } from "react-icons/ri";
import GlobalContext from "../context/GlobalContext";

const Modal = () => {
  const { isModalOpen, openModal, closeModal } = useContext(GlobalContext);

  return (
    <>
      <button
        className="bg-blue-200 text-black active:bg-blue-500 
            font-bold px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1"
        type="button"
        onClick={() => openModal()}
      >
        Open Weather Widget{" "}
      </button>
      {isModalOpen ? (
        <>
          <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0  z-50 outline-none focus:outline-none">
            <div className="relative  my-6 w-4/5 h-screen">
              <div className="border-0 mt-5 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                <div className="relative p-6 ">
                  <div className="flex items-start justify-end  rounded-t ">
                    <button
                      className="bg-transparent border-0 text-black float-right"
                      onClick={() => closeModal()}
                    >
                      <span className="cursor-pointer text-2xl">
                        <RiCloseCircleLine />
                      </span>
                    </button>
                  </div>
                  <WeatherWidget />
                </div>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
};

export default Modal;
