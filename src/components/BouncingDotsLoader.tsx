import React from "react";

const BouncingDotsLoader = (props) => {
  let circleCommonClasses = "h-2.5 w-2.5 bg-current rounded-full";

  return (
    <div className="flex items-center">
      <div className="font-bold text-xl -translate-y-[6px] mr-2">Loading</div>
      <div className={`${circleCommonClasses} mr-3 animate-bounce`}></div>
      <div className={`${circleCommonClasses} mr-3 animate-bounce200`}></div>
      <div className={`${circleCommonClasses} animate-bounce400`}></div>
    </div>
  );
};

export default BouncingDotsLoader;
