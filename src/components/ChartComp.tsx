import React from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);
export const options = {
  responsive: true,
  plugins: {
    title: {
      display: true,
      text: "forecast",
    },
  },
};

const ChartComp = ({ data }) => {
  return (
    <div className="w-full">
      <span
        className="text-xl m-2
      "
      >
        5 Days Forecast
      </span>
      <Line
        options={options}
        data={{
          labels: data.map((d) => d.label),
          datasets: [
            {
              label: "",
              data: data.map((d) => d.data),
              backgroundColor: "rgba(75,192,192,0.2)",
              borderColor: "#742774",
            },
          ],
        }}
      />
    </div>
  );
};

export default ChartComp;
