import React, { useEffect, useState } from "react";
import { BsWater } from "react-icons/bs";
import {
  RiSearchLine,
  RiCelsiusLine,
  RiEyeLine,
  RiTempHotLine,
  RiCloudWindyLine,
} from "react-icons/ri";
import ChartComp from "./ChartComp";
import WeatherForecastDay from "./WeatherForecastDays";
import axios from "axios";
import { ConvertToIcon } from "../utils/utils";
import BouncingDotsLoader from "./BouncingDotsLoader";

export interface LocationData {
  name?: string;
  country?: string;
  region?: string;
  lat?: string;
  lon?: string;
  timezone_id?: string;
  localtime?: string;
  localtime_epoch?: string;
  utc_offset?: string;
}

const WEATHER_API_KEY = "029055bd238d0f762f117f683ec577d3";
const IP_GEO_LOCATION_API_KEY = "d0e666aa662749dc8d0fb09f9c630378";

let days = ["Su", "Mo", "Tue", "Wed", "Thu", "Fr", "Sa"];

const WeatherWidget = () => {
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [location, setLocation] = useState<LocationData>({
    lat: "",
    lon: "",
    region: "",
  });

  const [data, setData] = useState<any>([]);
  const [errorMessage, setErrorMessage] = useState("");

  const handleChange = (event: any) => {
    setSearchTerm(event.target.value);
  };

  const handlerSubmit = (event: any) => {
    event.preventDefault();
    setLocation({ region: searchTerm });
  };

  useEffect(() => {
    axios
      .get(
        `https://api.geoapify.com/v1/ipinfo?&apiKey=${IP_GEO_LOCATION_API_KEY}`
      )

      .then(async ({ data }) => {
        let url_forecast = `https://api.openweathermap.org/data/2.5/forecast?lat=${data.location.latitude}&lon=${data.location.longitude}&appid=${WEATHER_API_KEY}`;
        let url_current_weather = `https://api.openweathermap.org/data/2.5/weather?lat=${data.location.latitude}&lon=${data.location.longitude}&appid=${WEATHER_API_KEY}`;
        const promise2 = axios.get(url_forecast);
        const promise1 = axios.get(url_current_weather);
        let output = [];

        Promise.all([promise1, promise2])
          .then((values) => {
            const [currentWeather, forecast] = values;

            forecast.data.list
              .map((li) => {
                return {
                  label: days[new Date(li.dt * 1000).getDay()],
                  data: [Math.round(parseInt(li.main.temp) - 273.15)],
                  icon: li.weather[0].main,
                };
              })
              // combine
              .forEach(function (item) {
                var existing = output.filter(function (v, i) {
                  return v.label === item.label;
                });
                if (existing.length) {
                  var existingIndex = output.indexOf(existing[0]);
                  output[existingIndex].data = output[
                    existingIndex
                  ].data.concat(item.data);
                } else {
                  if (typeof item.data === "string") item.data = [item.data];
                  output.push(item);
                }
              });

            // average of day
            const dayAvg = output.map((otp) => {
              return {
                ...otp,
                data: Math.round(
                  otp.data.reduce((acc, current) => {
                    return acc + current;
                  }) / otp.data.length
                ),
              };
            });

            setData({ currentWeather, weatherForecast: dayAvg });
          })
          .catch((err) => {
            setErrorMessage(err?.response?.data?.message);
          });
      });
  }, []);

  useEffect(() => {
    fetchWeatherData();
    // eslint-disable-next-line
  }, [location]);

  const fetchWeatherData = async () => {
    let url_forecast = ``;
    let url_current_weather;

    if (location?.region?.length > 0) {
      url_forecast = `https://api.openweathermap.org/data/2.5/forecast?appid=${WEATHER_API_KEY}&q=${location.region}`;
      url_current_weather = `https://api.openweathermap.org/data/2.5/weather?&q=${location.region}&appid=${WEATHER_API_KEY}`;
      const promise2 = axios.get(url_forecast);
      const promise1 = axios.get(url_current_weather);
      let output = [];

      Promise.all([promise1, promise2])
        .then((values) => {
          const [currentWeather, forecast] = values;

          forecast.data.list
            .map((li) => {
              return {
                label: days[new Date(li.dt * 1000).getDay()],
                data: [Math.round(parseInt(li.main.temp) - 273.15)],
                icon: li.weather[0].main,
              };
            })
            // combine
            .forEach(function (item) {
              var existing = output.filter(function (v, i) {
                return v.label === item.label;
              });
              if (existing.length) {
                var existingIndex = output.indexOf(existing[0]);
                output[existingIndex].data = output[existingIndex].data.concat(
                  item.data
                );
              } else {
                if (typeof item.data === "string") item.data = [item.data];
                output.push(item);
              }
            });

          // average of day
          const dayAvg = output.map((otp) => {
            return {
              ...otp,
              data: Math.round(
                otp.data.reduce((acc, current) => {
                  return acc + current;
                }) / otp.data.length
              ),
            };
          });

          setData({ currentWeather, weatherForecast: dayAvg });
        })
        .catch((err) => {
          setErrorMessage(err.response.data.message);
        });
    }
  };

  // remove error
  useEffect(() => {
    const timer = setTimeout(() => {
      setErrorMessage("");
    }, 3000);
    return () => clearTimeout(timer);
  }, [errorMessage]);

  if (!data.currentWeather || !data.weatherForecast) {
    return (
      <div className="w-full h-screen bg-gradientBg bg-no-repeat bg-cover bg-center flex flex-col justify-center items-center">
        <div>
          <BouncingDotsLoader />{" "}
        </div>
      </div>
    );
  }

  let icon = ConvertToIcon(data.currentWeather.data.weather[0].main);

  return (
    <div className="flex justify-center flex-col w-full ">
      <form className="flex justify-center  w-full" onSubmit={handlerSubmit}>
        <div className="relative w-4/12">
          <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none font-extrabold">
            <RiSearchLine />
          </div>
          <input
            className="block w-full focus:outline-offset-0	focus:outline-blue-300 p-4 pl-10 text-sm  border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            placeholder="Search"
            onChange={handleChange}
          />
          <button className="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
            Search
          </button>
        </div>
      </form>
      {errorMessage && (
        <span className=" bg-red-500 text-white  p-2 mt-2 text-center  capitalize rounded-md">{`${errorMessage}`}</span>
      )}
      <div className="flex items-center xl:flex-col ">
        <div className=" border border-gray-300  rounded-lg w-6/12 p-[1.19rem] m-3">
          <div className="my-5">
            <div className="flex items-center gap-x-5">
              <div className="text-[87px]">{icon}</div>
              <div>
                <div className="text-2xl font-semibold">
                  {data.currentWeather.data.name},{" "}
                  {data.currentWeather.data.sys.country}
                </div>
              </div>
            </div>
            <div className="flex justify-center items-center">
              <div className="text-[144px] leading-none font-light">
                {Math.round(
                  parseInt(data.currentWeather.data.main.temp) - 273.15
                )}
              </div>

              <div className="text-4xl">
                <RiCelsiusLine />
              </div>
            </div>

            <div className="capitalize text-center">
              {data.currentWeather.data.weather[0].description}
            </div>
          </div>

          <div className="max-w-[378px] mx-auto flex flex-col gap-y-6">
            <div className="flex justify-between">
              <div className="flex items-center gap-x-2">
                <div className="text-[20px]">
                  <RiEyeLine />
                </div>
                <div>
                  Visibility
                  <span className="ml-2">
                    {data.currentWeather.data.visibility / 1000} km
                  </span>
                </div>
              </div>
              <div className="flex items-center gap-x-5">
                <div className="text-[20px]">
                  <RiTempHotLine />
                </div>
                <div className="flex">
                  Feels like
                  <div className="flex ml-2">
                    {Math.round(
                      parseInt(data.currentWeather.data.main.feels_like) -
                        273.15
                    )}{" "}
                    <RiCelsiusLine></RiCelsiusLine>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex justify-between">
              <div className="flex items-center gap-x-2">
                <div className="text-[20px]">
                  <BsWater />
                </div>
                <div>Humidity</div>
                <span className="ml-2">
                  {data.currentWeather.data.main.humidity} %
                </span>
              </div>
              <div className="flex items-center gap-x-2">
                <div className="text-[20px]">
                  <RiCloudWindyLine />
                </div>
                <div>
                  Wind{" "}
                  <span className="ml-2">
                    {data.currentWeather.data.wind.speed} m/s
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="w-6/12 border border-gray-300 rounded-lg">
          <ChartComp data={data.weatherForecast} />
        </div>
      </div>
      <div>
        <WeatherForecastDay props={data.weatherForecast} />
      </div>
    </div>
  );
};

export default WeatherWidget;
