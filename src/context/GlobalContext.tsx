import React, { createContext, useState } from "react";

const GlobalContext = createContext(null);

export const GlobalProvider = ({ children }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const openModal = () => {
    setIsModalOpen(true);
  };
  const closeModal = () => {
    setIsModalOpen(false);
  };
  return (
    <div>
      <GlobalContext.Provider value={{ isModalOpen, openModal, closeModal }}>
        {children}
      </GlobalContext.Provider>
    </div>
  );
};

export default GlobalContext;
