import "./App.css";
import { GlobalProvider } from "./context/GlobalContext";
import Modal from "./components/Modal";

function App() {
  return (
    <GlobalProvider>
      <div className="App relative h-screen flex flex-col items-center justify-center bg-purple-200">
        <Modal />
      </div>
    </GlobalProvider>
  );
}

export default App;
